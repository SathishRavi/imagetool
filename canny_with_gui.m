function canny_with_gui()
clear all;
%im=zeros(256);
% figure(1);
f=figure('Position',[50,300,700,500],'Menubar','none','Name','GUI');
hsurf=uicontrol('Style','pushbutton','String','Update','Callback',@update_sigma,'Position',[570,220,100,25]);
hsurf1=uicontrol('Style','pushbutton','String','Select File','Callback',@loadimage_callback,'Position',[570,320,100,25]);
hsurf2=uicontrol('Style','pushbutton','String','Select Area','Callback',@selectarea_callback,'Position',[570,420,100,25]);

x1=40;
y1=60;
htext1=uicontrol('Style','text','String','Sigma','Position',[x1-10,y1,100,20]);
htext2=uicontrol('Style','text','String','ThrL','Position',[x1+90,y1,100,20]);
htext3=uicontrol('Style','text','String','ThrH','Position',[x1+190,y1,100,20]);

global hvalue1;
hvalue1=uicontrol('Style','edit','String','0.05','Callback',@update_sigma,'Position',[x1,y1-25,70,25]);
global hvalue2;
hvalue2=uicontrol('Style','edit','String','0.3','Min',0,'Max',1,'Callback',@update_thrL,'Position',[x1+100,y1-25,70,25]);
global hvalue3;
hvalue3=uicontrol('Style','edit','String','0.6','Min',0,'Max',1,'Callback',@update_thrH,'Position',[x1+200,y1-25,70,25]);


haxes=axes('Units','Pixels','Position',[30,160,500,300]);
%haxes1=axes('Units','Pixels','Position',[300,60,200,200]);

end 

function loadimage_callback(source,eventdata)
[hfile,impath]= uigetfile('*','String','Select File');
global im;
im=imread(fullfile(impath,hfile));
imshow(im);
end

function selectarea_callback(source,eventdata)
global im;

im=imcrop(im);

cannyedge();
end

function update_sigma(source,eventdata)
cannyedge();
end

function update_thrL(source,eventdata)
cannyedge();
end

function update_thrH(source,eventdata)
cannyedge();
end




function cannyedge()
global hvalue1;
global hvalue2;
global hvalue3;
global im;
i=im;

sig=str2double(hvalue1.String);


mn=str2double(hvalue2.String);

mx=str2double(hvalue3.String);
%i=double(imread('MyDog.jpg'));
%Addition of Gaussian Filter
h=fspecial('gaussian',5,sig);
igf=double(imfilter(i,h));

%Differentiation along x & y direction
%hx=[0 0 0;0 -1 1;0 0 0];
%hy=[0 0 0;0 -1 0;0 1 0];
hx=[-1 0 1;-2 0 2;-1 0 1];
hy=[1 2 1;0 0 0;-1 -2 -1];

gx=imfilter(igf,hx);
gy=imfilter(igf,hy);
magi=sqrt(gx.^2+gy.^2);
%magi=abs(gx)+abs(gy);
%Finding the Edge Direction:
theta=rad2deg(atan2(gy,gx));
for j=1:size(theta,1);
    for k=1:size(theta,2);
        if theta(j,k)<=22.5 & theta(j,k)>-22.5
            theta(j,k)=0;
        elseif theta(j,k)<=67.5 & theta(j,k)>22.5
            theta(j,k)=45;
        elseif theta(j,k)<=112.5 & theta(j,k)>67.5
            theta(j,k)=90;
        elseif theta(j,k)<=157.5 & theta(j,k)>112.5
            theta(j,k)=135;
        elseif theta(j,k)<=-22.5 & theta(j,k)>-67.5
            theta(j,k)=135;
        elseif theta(j,k)<=-67.5 & theta(j,k)>-112.5
            theta(j,k)=90; 
        elseif theta(j,k)<=-112.5 & theta(j,k)>-157.5
            theta(j,k)=45;
        else
             theta(j,k)=0;
        end;
   
    end
end

magn=zeros(size(magi));

%Non-Maxima Suppression
for j=1:size(magi,1);    
    for k=1:size(magi,2);
       %Based on the edge direction doing non-maxima suppression
       j1=j-1;
       j2=j+1;
       k1=k-1;
       k2=k+1;
       if j1<=0
           j1=2;
       end
       if k1<=0
           k1=2;
       end
       if j2>size(magi,1)
           j2=size(magi,1)-1;
       end
       if k2>size(magi,2)
           k2=size(magi,2)-1;
       end
      if theta(j,k)==0
        if magi(j,k) < magi(j,k1) || magi(j,k) < magi(j,k2)
         magn(j,k)=0;
        else
          magn(j,k)=magi(j,k);
        end
      elseif theta(j,k)==90
        if magi(j,k) < magi(j1,k) || magi(j,k) < magi(j2,k)
         magn(j,k)=0;
        else
          magn(j,k)=magi(j,k);
        end  
      elseif theta(j,k)==45
        if magi(j,k) < magi(j1,k2) || magi(j,k) < magi(j2,k1)
         magn(j,k)=0;
        else
          magn(j,k)=magi(j,k);
        end
      else theta(j,k)==135
        if magi(j,k) < magi(j1,k1) || magi(j,k) < magi(j2,k2)
         magn(j,k)=0;
        else
          magn(j,k)=magi(j,k);
        end  
      end
   end
end


%Thresholding
% mx=0.6;
% mn=0.2;
I1=im2bw(uint8(magn),mn);
I2=im2bw(uint8(magn),mx);
hsum=[1 1 1;1 0 1;1 1 1];
Isum=imfilter(I2,hsum);
Isumv=Isum(:);
%Edge Linking
Iedgev=I2(:);
I1v=I1(:);
for j=1:size(I1,1);
          
       if I1(j)==0
           Iedgev(j)=0;
       elseif Isum(j)==0
           Iedgev(j)=0;
       else
           Iedgev(j)=1;
       end     
end
Iedge=reshape(Iedgev,size(I1));
         
 figure(4);
 subplot(2,2,1),imshow(uint8(i));
% subplot(2,2,2),imshow(uint8(igf));
 subplot(2,2,2),imshow(uint8(magi));
 subplot(2,2,3),imshow(uint8(magn));


%figure(5);

%subplot(2,2,1),imshow(I1);
%subplot(2,2,2),imshow(I2);
subplot(2,2,4),imshow(Iedge);
%subplot(2,2,3),imshow(uint8(magn));
end

